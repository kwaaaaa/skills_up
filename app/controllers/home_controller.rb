class HomeController < ApplicationController
  before_action :authenticate_user!
  def index
  end

  def skill_up
    UserSkillUp.new(current_user, params).skill_up
    redirect_to root_path
  end

end
