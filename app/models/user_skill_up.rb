class UserSkillUp
  def initialize(user, params)
    @user = user
    @params = params
  end
  def skill_up
    if params_valid?
      ActiveRecord::Base.transaction do
        if character_xp_present?
          @user.update(xp: @params[:character_xp].to_i)
        end
        if character_strength_present?
          @user.update(strength: @params[:character_strength].to_i)
        end
        if character_agility_present?
          @user.update(agility: @params[:character_agility].to_i)
        end
        if character_stamina_present?
          @user.update(stamina: @params[:character_stamina].to_i)
        end
        'skills update'
      end
    else
      'invalid params'
    end
  end

  def params_valid?
    # this method summarizes the passed params and compare them to user fields
    require_value = 0
    real_value = 0
    if character_xp_present?
      require_value += @params[:character_xp].to_i
      real_value += @user.xp.to_i
    end
    if character_strength_present?
      require_value += @params[:character_strength].to_i
      real_value += @user.strength.to_i
    end
    if character_agility_present?
      require_value += @params[:character_agility].to_i
      real_value += @user.agility.to_i
    end
    if character_stamina_present?
      require_value += @params[:character_stamina].to_i
      real_value += @user.stamina.to_i
    end
    require_value == real_value
  end

  def character_xp_present?
    @params[:character_xp].to_i != 0
  end

  def character_strength_present?
    @params[:character_strength].to_i != 0
  end

  def character_agility_present?
    @params[:character_agility].to_i != 0
  end

  def character_stamina_present?
    @params[:character_stamina].to_i != 0
  end
end