// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or any plugin's vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require jquery_ujs
//= require turbolinks
//= require_tree .
//= require bootstrap-sprockets

$(document).on("click", ".increment", function(e) {
  changeSkills(e);
});

$(document).on("click", ".decrement", function(e) {
  changeSkills(e);
});

function changeSkills(e){
  var eventId = $(e.target).attr('id');
  var skill = document.getElementsByClassName(eventId);
  var hiddenSkill = document.getElementById(eventId);
  var skillValue = skill[0].innerHTML;
  var xpValue = ($(".character_xp").text());

  if (xpValue > 0) {
    if ($(e.target).attr('class').indexOf('increment') > -1){
      skillValue++;
      xpValue--;
    }
    if ($(e.target).attr('class').indexOf('decrement') > -1 && skillValue > 0){
      skillValue--;
      xpValue++;
    }
    skill[0].innerHTML = skillValue;
    hiddenSkill.value = skillValue;
    $(".character_xp").text(xpValue);
    $("#character_xp").val(xpValue);
  }
  else {
    alert('xp less zero!');
  };
};

