# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

if User.count == 0
  User.create!(email: 'user@example.com', password: 'Password', password_confirmation: 'Password',
               strength: 10, agility: 15, stamina: 20, xp: 100)
  User.create!(email: 'user2@example.com', password: 'Password', password_confirmation: 'Password',
               strength: 0, agility: 0, stamina: 0, xp: 100)
  User.create!(email: 'user3@example.com', password: 'Password', password_confirmation: 'Password',
               strength: 100, agility: 150, stamina: 200, xp: 1000)
end