class AddFieldsToUser < ActiveRecord::Migration
  def change
    add_column :users, :strength, :integer, default: 0, null: false
    add_column :users, :agility, :integer, default: 0, null: false
    add_column :users, :stamina, :integer, default: 0, null: false
    add_column :users, :xp, :integer, default: 0, null: false
  end
end
